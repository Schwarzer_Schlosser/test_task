import time
import statistics


def get_max_min_sequence(lst, is_max=True):
    count_seq = 0
    sequence = []

    for number in lst:
        if not sequence:
            sequence.append(number)
        else:
            if is_max and number > sequence[-1]:
                sequence.append(number)
            elif not is_max and number < sequence[-1]:
                sequence.append(number)
            else:
                sequence.clear()
        count_seq = len(sequence) if len(sequence) > count_seq else count_seq

    return count_seq if count_seq > 1 else 0


start = time.time()

with open("10m.txt") as f:
    numbers = list(map(int, f.read().strip().split('\n')))

min_number = min(numbers)
max_number = max(numbers)
avg_value = sum(numbers) / len(numbers)
median = statistics.median(numbers)
max_sequence = get_max_min_sequence(numbers, is_max=True)
min_sequence = get_max_min_sequence(numbers, is_max=False)

print(max_number)
print(min_number)
print(avg_value)
print(median)
print(max_sequence)
print(min_sequence)


result = time.time() - start
print(f'Program execution time: {result:.2f} second.')
